#!/bin/bash

COCKROACHDB_HOST=${COCKROACHDB_HOST:-"cockroachdb-public.qaclana-infra.svc"}
WAIT_TIMEOUT=${WAIT_TIMEOUT:-"60"}

total_wait=0
while true
do
    /cockroach/cockroach node status --insecure --host ${COCKROACHDB_HOST} > /dev/null 2>&1
    if (( $? == 0 )); then
        break
    else
        if (( total_wait >= ${WAIT_TIMEOUT} )); then
            echo "Timed out waiting for CockroachDB."
            exit 1
        fi
        echo "CockroachDB is still not up at ${COCKROACHDB_HOST}. Waiting 1 second."
        sleep 1s
        ((total_wait++))
    fi
done

echo "Generating the schema"
for file in $(ls /sql) ;
do
    echo "Executing ${file}"
    /cockroach/cockroach sql --insecure --host ${COCKROACHDB_HOST} < /sql/${file}
    if (( $? == 0 )); then
        echo "${file} succeeded"
    else
        echo "${file} FAILED"
    fi
done
echo "Done."
