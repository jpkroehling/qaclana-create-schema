CREATE DATABASE IF NOT EXISTS qaclana;

CREATE TABLE IF NOT EXISTS qaclana.kv (
    key varchar(100) not null,
    value varchar(100) null
);
