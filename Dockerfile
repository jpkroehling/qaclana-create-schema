FROM centos:centos7

RUN mkdir -p /cockroach && \
    curl https://binaries.cockroachdb.com/cockroach-latest.linux-amd64.tgz -o /cockroach-latest.linux-amd64.tgz && \
    tar xzf /cockroach-latest.linux-amd64.tgz --directory=/cockroach --strip 1

COPY create-schema.sh /
ADD sql /sql/

ENV COCKROACHDB_HOST=cockroachdb-public.qaclana-infra.svc
CMD ["/create-schema.sh"]
